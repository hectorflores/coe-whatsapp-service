FROM maven:3-jdk-8
ADD target/coe-whatsapp-service.jar coe-whatsapp-service.jar
EXPOSE 8090
ENTRYPOINT ["java","-jar","coe-whatsapp-service.jar"]
