package com.coe.microservice.whatsappservice.controller;

import com.coe.microservice.whatsappservice.service.WhatsappService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600, allowCredentials = "false")
@RestController
public class WhatsappController {

    @Autowired
    private WhatsappService whatsappService;

    @GetMapping("/whatsapp/send")
    public void sendWhatsapp() {
        try {
            whatsappService.sendWhatsapp();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
