package com.coe.microservice.whatsappservice.service;

import org.springframework.stereotype.Component;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;

@Component
public class WhatsappService {
    private static final String ACCOUNT_SID = "ACc4dfd10e307f1a5d18abe8757b50bee2";
    private static final String AUTH_TOKEN = "9159bd2c3c7262534316c660ea746328";

    public void sendWhatsapp(){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(
                new com.twilio.type.PhoneNumber("whatsapp:+5214491510858"),
                new com.twilio.type.PhoneNumber("whatsapp:+14155238886"),
                "The event has been added")
                .create();

        System.out.println(message.getSid());
    }
}
